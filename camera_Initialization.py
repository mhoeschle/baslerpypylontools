import pypylon.pylon as py
from pypylon import pylon
import numpy as np
import matplotlib.pyplot as plt
from pypylon import genicam


###
#
# All functions to read and write the toml files
# All functions are executed in basler_camera_access.py file via import
#
###

def get_instance_tlf(pylon):
    '''
    get the TransportLayerFactory in TLF instances. List all connected devices (Cameras) in DEVICES
    '''
    # get instance of the pylon TransportLayerFactory
    TLF = pylon.TlFactory.GetInstance()

    # LIST all connected devices (Cameras) in DEVICES (CDeviceInfo) and exit application if no device is found.
    DEVICES = TLF.EnumerateDevices()
    if len(DEVICES) == 0:
        raise pylon.RuntimeException("No camera present.")
    # return TransportLayerFactory
    return DEVICES, TLF

#
# connect camera and store connected devices in DEVICES
# DEVICES, TLF = get_instance_tlf(pylon)

# READ the the serial and swig from the connected cameras and write it to lists:


def find_camera_serial_swig(DEVICES, TLF):
    '''
    Find the the serial and the SWIG adress of each connected cameras and write it into a list and a dict
    '''
    # save the camera information into list and dictionary - needed??
    cam_instances = []  # swig - Pylon::CInstantCamera at address 0x0.....
    cam_serials = []

    cam_serial_swig_adress = {}

    # iterate and find all connected cameras:

    for i, camera_device in enumerate(DEVICES):
        print('----------- Start ------------------')
        # print model name and id of the camera
        print(f'Camera Model: {camera_device.GetModelName()}')
        print(f'Serial numer: {camera_device.GetSerialNumber()}', "\n")

        # the active camera will be an InstantCamera (CInstantCamera) based on a device to access the parameter
        # created with the corresponding DeviceInfo
        # this can be from a list of previously enumerated
        cam = py.InstantCamera(TLF.CreateDevice(DEVICES[i]))
        print(f'CAM_SWIG: {cam}')  # - Debug
        print(type(cam), '\n')  # - Debug

        # read out all camera information
        pylon.FeaturePersistence.Save("test.txt", cam.GetNodeMap())

        # print(f'Nr: {i} - cam: {cam} ') # - Debug

        # add cameras Instances as swig to empty list
        cam_instances.append(cam)

        # add camera Instances and serial id to empty list
        cam_serials.append(camera_device.GetSerialNumber())

        # create dict with swig and camera serial id
        cam_serial_swig_adress[cam] = camera_device.GetSerialNumber()

    # return dict with camera serials and their swig adress
    return cam_serial_swig_adress


# cam_serial_swig_adress = find_camera_serial_swig(DEVICES, TLF)
