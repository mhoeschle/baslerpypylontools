import pypylon.pylon as py
from pypylon import pylon
import numpy as np
import matplotlib.pyplot as plt
from pypylon import genicam
import sys
import time
import os
import tomli as tomllib
import toml
import camera_Initialization as ci
import toml_handling as th
import camera_controler as cam_control


# connect camera and store connected devices in DEVICES
DEVICES, TLF = ci.get_instance_tlf(pylon)

# READ the the serial and swig from the connected cameras and write it to lists:
cam_serial_swig_adress = ci.find_camera_serial_swig(DEVICES, TLF)


# -------- for toml file -------------

# If you run via cmd choose the correct file, if you run with visual code - takes default toml file
# only running without a specific toml file it will use the default one, adding a toml file after the script name it will choose the entered one!


config_filename = th.checktoml(sys.argv)
# print(f'Config_filename: {config_filename}') # for debugging

cam_data_update = th.access_toml_file(cam_serial_swig_adress)


print()
print("----------cameras found and access parameters saved, read from updated toml file, choose the correct cameras.  --------------", '\n')

# get the camera id's from the camera you want to make the changes from the camera_parameters_config.toml file:
for camera_serial_toml in cam_data_update.get('camera_ids').values():
    # print(camera_serials) # for debuging

    # from the camera_parameters_config.toml take the camera id's you want to change and take the swig of the camera from

    for cam_swig, cam_serial in cam_serial_swig_adress.items():
        if int(camera_serial_toml) == int(cam_serial):
            print(
                f'YES, camera found: {camera_serial_toml} = {cam_serial} + {cam_swig}')
            print(type(cam_swig))

            # Add camera controler here to exectue on the camera!
            # open camera
            cam_control.opencam(cam_swig, cam_serial)

            # read Gain, Trigger delay, exposure, fps and gamma from camera
            cam_control.readcam(cam_swig, cam_serial)

            # change gain with value from toml file:
            cam_control.gaincam(cam_swig, cam_serial, cam_data_update)

            # change exposure with value from toml file:
            cam_control.exposurecam(cam_swig, cam_serial, cam_data_update)

            # change delay of the trigger (exposure) with value from toml file in milliseconds (ms):
            cam_control.triggerdelaycam(cam_swig, cam_serial, cam_data_update)

            # close camera
            cam_control.closecam(cam_swig, cam_serial)

        else:
            print(f'{camera_serial_toml}: NO, not found')

    # readcam(swig, camera_serials)

# print('------------- Stop ---------------')


# if __name__ == '__main__':
    # :'read': run on cmd  # :sys.argv[1]: to run in visual code

    # check_cmd_input_file(argv_1, default_config_filename)
