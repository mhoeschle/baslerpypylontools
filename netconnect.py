import smbclient
from smbprotocol.exceptions import LogonFailure, SMBOSError
import getpass
from smbclient.path import isdir, exists
import BaslerChangeParameter

'''
Code connect to 2 DVR's and make temporary changes for the Basler camera, only for calibration mode
Steps:
    Read out existing cameras values
    change values
    recording via external Software
    stop calibration recording
    close connection
'''


def connectnet():
    """
    Enter username and password to connect to server
    """

    # Ask for login name of the user
    user = input("Please, enter username: ")

    # username = str('mpi\\' + user)
    username = str(user)
    print(f'Hello {user}')

    # The getpass module for inserting password
    password = getpass.getpass("Please enter your password: ")

    # Get server name to connect to: for e.g. "ps-access.is.localnet"
    # server = input("Please enter the servername or IP to connect to: ")

    # Setup Test computer and connect basler cam to it, to test connection and execution
    servers = {
        '10.34.22.41': '191031-01handU',
        # '10.34.25.94': 'DESKTOP-8KL64PU',
        # '10.34.22.31': 'DVR12',
        # '10.34.22.32': 'DVR13',
        # '10.34.22.33': 'DVR14',
        # '10.34.22.34': 'DVR15'
    }

    # Optional - register the credentials with a server (overrides ClientConfig for that server)
    # smbclient.register_session("server", username="user", password="pass")
    for server in servers:
        # For testing - D:\ is external HHD

        # If no Harddrive is needed to run the code this lines shouldn't be activated!
        # LOCALDIRECTORIES = [f"\\\\{server}\\c\\"]
        # ['C:\\', 'U:\\', 'V:\\', 'W:\\', 'X:\\', 'Y:\\', 'Z:\\'] for the DVR's

        try:
            # Connect to server:
            smbclient.register_session(server, username, password)
            print(f'Connected!! to: {server}')

            # Test on DVR 1 Hand scanner
            # smbclient.mkdir(
            # f"\\\\{server}\\c$\\Users\\3dmd\\Desktop\\test12345")

            # Start script to for camera manipulation with pypylone:
            # List current current cameras
            # BaslerChangeParameter.listcam()

            # connect to the following cameras
            # BaslerChangeParameter.opencam()

            # directories = smbclient.listdir(f"\\\\{server}\\c\\")
            # print(directories)
            # smbclient.(f"\\\\{server}\\c\\")

        # debug
        # break
        except LogonFailure as e:
            print(f"Login error: \n\t {e}")
        except SMBOSError as e:
            print(f"OS error: \n\t {e}")

    print(f'close server: {server}')


if __name__ == "__main__":
    connectnet()
