###
#
# All functions to read and write the basler camera access file
# All functions are executed in basler_camera_access.py file via import
#
###

# open cam

def opencam(cam_swig, cam_serial):
    '''
    open camera to access camera and read or write values
    '''
    cam_swig.Open()
    print(f'Cam open: {cam_serial}', '\n')


# opencam(cam_swig, cam_serial)

# close cam

def closecam(cam_swig, cam_serial):
    '''
    close camera after read or write values
    '''
    cam_swig.Close()
    print(f'Cam closed: {cam_serial}', '\n')
# closecam(cam_swig, cam_serial)


def readcam(cam_swig, cam_serial):
    """
    reads out current settings from camera via interface
    """
    # Read Gain value in Camera and round to two decimal points :
    gain = cam_swig.Gain.GetValue()
    gain = round(gain, 2)
    print(
        f'Gain in (dB): {gain} from camera: {cam_serial}')

    # Read Trigger Delay value in Camera:
    print(
        f'trigger delay from camera (in Microseconds) {cam_serial} is: {cam_swig.TriggerDelay.GetValue()}')

    # Exposure parameter
    print(
        f'old exposure in microseconds: {cam_swig.ExposureTime.GetValue()} from camera: {cam_serial}')

    # fps
    oldfps = cam_swig.AcquisitionFrameRate.GetValue()
    print(f'fps: {oldfps} from camera: {cam_serial}')

    # Gamma value:
    print("Camera gamma value: ")
    print(cam_swig.Gamma.GetValue(), "\n")


# readcam(cam_swig, cam_serial)

def gaincam(cam_swig, cam_serial, cam_data_update):
    """
        changes the gain in (dB). Entering float numbers for. e.g: 5.0
    """
    gainvalue = cam_data_update.get('camera_parameters')['gain']

    # Input new gain value
    # gainvalue = float(input("Please enter the new gain in (dB): "))
    # Set new value to camera
    cam_swig.Gain.SetValue(gainvalue)

    # Read Gain value in Camera:
    updated_gain = cam_swig.Gain.GetValue()
    updated_gain = round(updated_gain, 2)
    print(
        f'Updated_Gain in (dB): {updated_gain} from camera: {cam_serial}')


# cam_control.gaincam(cam_swig, cam_serial, cam_data_update)


#
# Funktion for changing the exposure time in miliseconds (e.g. 1000 ms):
#


def exposurecam(cam_swig, cam_serial, cam_data_update):
    """
        changes the exposure time in miliseconds (ms)
    """

    # new value from toml file
    exposure_value = cam_data_update.get('camera_parameters')['exposure']

    # add
    new_exposure = float(exposure_value)
    cam_swig.ExposureTime.SetValue(new_exposure)

    # Read new exosure value in Camera:
    print(
        f'Updated_Exposure: {cam_swig.ExposureTime.GetValue()} from camera: {cam_serial}')


# exposurecam(cam_swig, cam_serial, cam_data_update)

#
# Funktion for changing the delay of the trigger in miliseconds (e.g. 1000 ms):
#

def triggerdelaycam(cam_swig, cam_serial, cam_data_update):
    """
        changes the trigger delay in miliseconds (ms)
    """
    # new value from toml file
    delay_value = cam_data_update.get('camera_parameters')['delay']

    cam_swig.TriggerDelay.SetValue(float(delay_value))

    print(
        f'New trigger delay from camera {cam_swig.TriggerDelay.GetValue()} is: {cam_serial}')

# triggerdelaycam(cam_swig, cam_serial, cam_data_update)
