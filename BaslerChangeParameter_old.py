import pypylon.pylon as py
from pypylon import pylon
import numpy as np
import matplotlib.pyplot as plt
from pypylon import genicam
import sys
import time
import os


#
# Variable:
#

# Max Number of cameras that should be used
MAX_CAMERAS_TO_USE = 2


# get instance of the pylon TransportLayerFactory
TLF = pylon.TlFactory.GetInstance()

# LIST all connected devices (Cameras) in DEVICES (CDeviceInfo) and exit application if no device is found.
DEVICES = TLF.EnumerateDevices()
if len(DEVICES) == 0:
    raise pylon.RuntimeException("No camera present.")

# Create an array of instant cameras for the found devices and avoid exceeding a maximum number of devices. -> Output-> Pylon::CInstantCameraArray
# CAMERAS_ARRAY = pylon.InstantCameraArray(min(len(DEVICES), MAX_CAMERAS_TO_USE))
# print(f'cameras: {CAMERAS_ARRAY}')  # for debug
# l = CAMERAS_ARRAY.GetSize()

#
# Funktions:
#

#
# List all connected / founded cameras:
#


def listcams(method_name):
    '''
    Iterating through all cameras. By adding an argument you can read or write values to the camera.
    ---------------------------
    Following arguments usable:
    ---------------------------
    :gain: increase the brightness of the image output
    :delay: allows to determine the period of time between the detection of the trigger signal and the actual start of the exposure
    :exposure: specifies how long the image sensor is exposed to light during image acquisition.
    :read: reads out all the parameters that can be used as arguments 
    :fps: allows you to determine the estimated frame rate with the current camera settings.

    '''

    # save the camera information into list and dictionary - needed??
    cam_instances = []
    cam_instances_dic = {}

    # itterates through all connected cameras and select them step by step after each other
    # camera_device is only

    for i, camera_device in enumerate(DEVICES):
        print('----------- Start ------------------')
        # print model name and id of the camera
        print(f'Camera Model: {camera_device.GetModelName()}')
        print(f'Serial numer: {camera_device.GetSerialNumber()}', "\n")

        # the active camera will be an InstantCamera (CInstantCamera) based on a device to access the parameter
        # created with the corresponding DeviceInfo
        # this can be from a list of previously enumerated
        cam = py.InstantCamera(TLF.CreateDevice(DEVICES[i]))
        # print(f'CAM: {cam}') # - Debug

        # print(f'Nr: {i} - cam: {cam} ') # - Debug

        # open camera -> :cam: is the camera instance to access camera, :camera_device: the camera info like serial number, ...
        opencam(cam, camera_device)

        # Load toml as a dictionary
        # config = toml.load ...

        # choose the method you want to make changes
        if method_name == 'gain':
            gaincam(cam, camera_device, config['gainvalue'])
        elif method_name == 'read':
            readcam(cam, camera_device)
        elif method_name == 'delay':
            triggerdelaycam(cam, camera_device)
        elif method_name == 'exposure':
            exposurecam(cam, camera_device)
        else:
            print('wrong argumnets. Please choose: gain, read, delay, exposure, fps')

        # close cam
        closecam(cam, camera_device)

        # Listing of Instance and camera info is not required - maybe for later usage!!

        # add cameras Instances to list
        cam_instances.append(cam)
        # add camera Instances and serial id to a list
        cam_instances_dic[camera_device.GetSerialNumber()] = cam

    # print(f'Cam_dict: {cam_instances_dic}') # - debug
    print('------------- Stop ---------------')

# CAM = testcam()


def opencam(cam, camera_device):
    '''
    open camera to access camera and read or write values
    '''
    cam.Open()
    print(f'Cam open: {camera_device.GetSerialNumber()}', '\n')

# Close cameras:


def closecam(cam, camera_device):
    '''
    close camera after read or write values
    '''
    cam.Close()
    print(f'Cam closed: {camera_device.GetSerialNumber()}', '\n')


# read camera parameters to get an current overiew

def readcam(cam, camera_device):
    """
    reads out current settings from camera via interface
    """
    # Read Gain value in Camera and round to two decimal points :
    gain = cam.Gain.GetValue()
    gain = round(gain, 2)
    print(
        f'Gain in (dB): {gain} from camera: {camera_device.GetSerialNumber()}')

    # Read Trigger Delay value in Camera:
    print(
        f'trigger delay from camera (in Microseconds) {camera_device.GetSerialNumber()} is: {cam.TriggerDelay.GetValue()}')

    # Exposure parameter
    print(
        f'old exposure in microseconds: {cam.ExposureTime.GetValue()} from camera: {camera_device.GetSerialNumber()}')

    # fps
    oldfps = cam.AcquisitionFrameRate.GetValue()
    print(f'fps: {oldfps} from camera: {camera_device.GetSerialNumber()}')

    # Gamma value:
    print("Camera gamma value: ")
    print(cam.Gamma.GetValue(), "\n")


# readcam()

#
# Funktion for changing the value (uncomment the comment in the next line (camera.Gain = YourValue))
#


def gaincam(cam, camera_device=None, gainvalue=None):
    """
        changes the gain in (dB). Entering float numbers for. e.g: 5.0
    """
    # Read Gain value in Camera:
    print(
        f'Gain in (dB): {cam.Gain.GetValue()} from camera: {camera_device.GetSerialNumber()}')

    # Input new gain value
    # gainvalue = float(input("Please enter the new gain in (dB): "))
    # Set new value to camera
    cam.Gain.SetValue(gainvalue)

    # Read Gain value in Camera:
    new_gain = cam.Gain.GetValue()
    new_gain = round(new_gain, 2)
    print(
        f'New Gain in (dB): {new_gain} from camera: {camera_device.GetSerialNumber()}')


# gaincam()

#
# Funktion for changing the exposure time in microseconds (e.g. 1000 ms):
#


def exposurecam(cam, camera_device=None):
    """
        changes the exposure time in microseconds (ms)
    """

    # read current value
    old_exposure = cam.ExposureTime.GetValue()
    print(
        f'old exposure in microseconds: {old_exposure} from camera: {camera_device.GetSerialNumber()}')
    # ask user to add new value
    new_exposure = input("Please enter new exposure value (microseconds): ")
    new_exposure = float(new_exposure)
    cam.ExposureTime.SetValue(new_exposure)

    # Read new exosure value in Camera:
    print(
        f'New exposure: {cam.ExposureTime.GetValue()} from camera: {camera_device.GetSerialNumber()}')


# exposurecam()

#
# Funktion for changing the aquisation frame rate in fps (e.g. 30 fps):
#


def aquisationfps(cam, ):
    """
        enable / dissable auto or manual fps and
        changes the fps (frame per seconds)
    """

    # read current value
    status = cam.AcquisitionFrameRateEnable.GetValue()
    print(f'fps enable? {status}')
    # ask user to change or not
    statusenable = str(input("Do you want to change fps, (y/n)? "))

    if statusenable.lower() == 'y':
        cam.AcquisitionFrameRateEnable.SetValue(True)
        oldfps = cam.AcquisitionFrameRate.GetValue()
        print(f'Old fps: {oldfps}')

        newfps = input("Choose your fps: ")
        cam.AcquisitionFrameRate.SetValue(float(newfps))

        print(f"New fps is: {newfps}", '\n')

    else:
        print("disabled Aquisation Frame rate!")
        cam.AcquisitionFrameRateEnable.SetValue(False)

# aquisationfps()

#
# Funktion for changing the trigger delay in microseconds (e.g. 2000 ms ):
# For e.g. Useful for running 2 different cameras and a delay needs to be added due to using same trigger signal.
#


def triggerdelaycam(cam, camera_device=None):
    """
        changes the trigger delay in microseconds (ms)
    """

    print(
        f'trigger delay from camera (in Microseconds) {camera_device.GetSerialNumber()} is: {cam.TriggerDelay.GetValue()}')

    newtriggerdelay = input(
        "Please enter new trigger delay in (microseconds): ")

    cam.TriggerDelay.SetValue(float(newtriggerdelay))

    print(
        f'New trigger delay from camera {camera_device.GetSerialNumber()} is: {cam.TriggerDelay.GetValue()}')

# triggerdelaycam()

# How to show the images ?
# Next step
# start canera in free run mode:


def freeruncam(cam):
    '''
# grabbing images in free run
'''

    # camera start free run mode: VIDEO 37:32
    cam.StartGrabbing(pylon.GrabStrategy_OneByOne)
    i = 0
    print('Starting to acquire')
    t0 = time.time()
    while cam.IsGrabbing():
        grab = cam.RetrieveResult(
            100, pylon.TimeoutHandling_ThrowException)
        if grab.GrabSucceeded():
            i += 1
        if i == 100:
            cam.StopGrabbing()
            break

    print(f'Acquired {i} frames in {time.time()-t0:.0f} seconds')


# ---> ongoing, not yet in usage.

# CAM_ID, CAM_INSTANT = listcams()

if __name__ == '__main__':
    import sys
    # :'read': run on cmd  # :sys.argv[1]: to run in visual code
    method_name_input = sys.argv[1]
    listcams(method_name=method_name_input)
