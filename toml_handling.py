import sys
import tomli as tomllib
import toml

###
#
# All functions to read and write the toml files
# All functions are executed in basler_camera_access.py file via import
#
###

# -------- for toml file -------------

# If you run via cmd choose the correct file, if you run with visual code - takes default toml file
# only running without a specific toml file it will use the default one, adding a toml file after the script name it will choose the entered one!


def checktoml(cmd_input):
    '''
    Running script via command line, gives you the possability to choose different configfiles with different parameters
    Sys.argv[0] = scipt name, sys.argv[1] = config file name
    Example how to run with Command line:. python scriptname.py configfilename.toml
    '''
    default_file = (r'config_files\camera_parameters_config.toml')
    # chosen_file = tomlfilename[1]

    if len(cmd_input) < 1:
        # print(f'tomlfilename: {tomlfilename}') # for debugging
        # exit(1)
        return default_file

    elif len(cmd_input) > 1:
        tomlfile = cmd_input[1]
        # print(f'tomlfile: {tomlfile}') # for debugging
        return tomlfile
    else:
        # print(f'default_File: {default_file}') # for debugging
        return default_file


config_filename = checktoml(sys.argv)
# print(f'Config_filename: {config_filename}') # for debugging

#
# open toml file:
#


def access_toml_file(cam_serial_swig_adress):
    '''
    open toml file for read and write the config parameter from or to the camera
    '''
    #
    # open the toml file
    #
    with open(config_filename, "rb") as f:
        data = tomllib.load(f)

    #
    # read toml files:
    #
    for parameter in data.items():
        print(f'Parameter_Initialisation: {parameter}')  # for debug

    #
    # delete connected cameras to refresh each time new:
    #
    data['connected_cameras'] = {}

    #
    # Modify values in the toml config under connected_cameras with explored cameras infos from the script:
    #
    for cam_instance, serial in cam_serial_swig_adress.items():
        data['connected_cameras'][serial] = cam_instance

    # for parameter in data.items():
        # print(f'Parameter_after_updating: {parameter}')

    # change data to update befor uploading to the file
    cam_data_update = data

    #
    # Write to modified config:
    #
    # move to the end
    with open(config_filename, "w") as f:
        toml.dump(data, f)

    return cam_data_update

# access_toml_file(cam_serial_swig_adress)

# debug:
# test = cam_data_update.get('camera_ids').get('camera_id_2')
# print(f'Camera_ID: {test}')

# test2 = list(cam_data_update.get('connected_cameras').keys())[0]
# print(f'Camera_ID_Updated: {test2}')

# split into two files one read and other write
