### Hi there 👋, my name is Markus

#### BaslerChangeParameters 

I made this project just for fun, it allows you to read out and change any settings in the basler cameras.
This should be helpful if you are using more than one camera and you want to make changes to several ones at once, same parameter and value.

##### Execute Script

Execute the scipt via cmd. BaslerChangeParameters.py 'argument' be sure to be in the same folder as the scipt. The argument could be any toml file that follows the template "camera_parameters_config.toml".
for e.g. python basler_camera_access.py tomlfile.toml

**arguments to choose:**
:**gain**: increase the brightness of the image output
:**delay**: allows to determine the period of time between the detection of the trigger signal and the actual start of the exposure
:**exposure**: specifies how long the image sensor is exposed to light during image acquisition.
:**read**: reads out all the parameters that can be used as arguments 
:**fps**: allows you to determine the estimated frame rate with the current camera settings.


##### Next steps

- create a template for toml files

-Connect to several computers via network and get acces to other cameras to change the settings in those cameras too by using netconnect.py


Skills: python, cmd, 

- 🔭 I’m currently working on this page. 
